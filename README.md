
# Home Environment Builder

This repository simply contains my default home environment files and
a makefile to apply them properly. The makefile also builds **vim** from
source and installs it locally for the current user.

