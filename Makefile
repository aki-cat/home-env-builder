
PREFIX=$$HOME/local
OPENSRC_PATH=$$HOME/source

GITCONFIG_SOURCE=.gitconfig
GITCONFIG=$$HOME/.gitconfig

VIMRC_SOURCE=.vimrc
VIMRC=$$HOME/.vimrc

BASHRC_SOURCE=.bashrc
BASHRC=$$HOME/.bashrc

VIMPLUG=$$HOME/.vim/autoload/plug.vim
VIMPLUG_SRC=https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

LUAVER_PATH=${OPENSRC_PATH}/luaver
LUAVER_REPO=https://github.com/DhavalKapil/luaver.git
LUAVER_TAG=v1.1.0
LUAVER=$$HOME/.luaver/luaver

LUA_VERSION=5.1
LUA_PREFIX=$$HOME/.luaver/lua/${LUA_VERSION}
LUA_BIN=${LUA_PREFIX}/bin/lua

LUAJIT_VERSION=2.0.5
LUAJIT_PREFIX=$$HOME/.luaver/luajit/2.0.5
LUAJIT_BIN=${LUAJIT_PREFIX}/bin/luajit

VIM_PATH=${OPENSRC_PATH}/vim
VIM_REPO=https://github.com/vim/vim.git
VIM_TAG=v8.0.1655
VIM_FLAGS=--with-features=huge --enable-multibyte \
		--enable-perlinterp=yes --enable-rubyinterp=yes \
		--enable-luainterp=yes --with-lua-prefix=${LUA_PREFIX} \
		--with-luajit=yes --with-luajit-prefix=${LUAJIT_PREFIX} \
		--prefix=${PREFIX}
VIM_BIN=${PREFIX}/bin/vim


# COMMANDS

.PHONY: all validate clean check-env
all: check-env $$HOME ${BASHRC} ${VIMRC} ${VIMPLUG} \
	${LUAVER} ${LUA_BIN} ${LUAJIT_BIN} ${VIM_BIN} ${GITCONFIG}

clean: check-env
	rm -rf ${BASHRC} ${VIMRC} ${LUAVER_PATH} \
		${LUA_PREFIX} ${LUAJIT_PREFIX} \
		${VIM_PATH} ${VIM_BIN}

validate: check-env
	@if [ ! -e ${BASHRC} ]; then echo "'${BASHRC}' missing"; false; fi
	@if [ ! -e ${VIMRC} ]; then echo "'${VIMRC}' missing"; false; fi
	@if [ ! -e ${LUAVER} ]; then echo "'${LUAVER}' missing"; false; fi
	@if [ ! -e ${VIM_BIN} ]; then echo "'${VIM_BIN}' missing"; false; fi
	@echo "Everything OK."

check-env:
	@echo "Home directory set to '$$HOME'"
	@echo "Shell is '$$SHELL'"


# DEPENDENCIES

## HOME BUILDING

$$HOME:
	mkdir -p $$HOME

${PREFIX}:
	mkdir -p ${PREFIX}

${OPENSRC_PATH}:
	mkdir -p ${OPENSRC_PATH}


## BASH AND VIM CONFIG FILES

${BASHRC}:
	cp -pv ${BASHRC_SOURCE} ${BASHRC}

${VIMRC}: ${VIMPLUG}
	cp -pv ${VIMRC_SOURCE} ${VIMRC}

${GITCONFIG}:
	cp -pv ${GITCONFIG_SOURCE} ${GITCONFIG}

## VIM PLUG

${VIMPLUG}:
	curl -fLo ${VIMPLUG} --create-dirs ${VIMPLUG_SRC}


## LUA

${LUAVER}: ${LUAVER_PATH}/.git ${BASHRC}
	SHELL=$$SHELL ${LUAVER_PATH}/install.sh

${LUAVER_PATH}/.git:
	mkdir -p ${LUAVER_PATH}
	cd ${LUAVER_PATH}; \
		if [ ! -e .git ]; then \
			git clone ${LUAVER_REPO} .; \
		fi; \
		git checkout ${LUAVER_TAG};

${LUA_BIN}: ${LUAVER}
	echo 'y'$'\n''y'$'\n' | ${LUAVER} install 5.1 # auto confirm

${LUAJIT_BIN}: ${LUAVER}
	echo 'y'$'\n''y'$'\n' | ${LUAVER} install-luajit 2.0.5 # auto confirm


## ACTUAL VIM

${VIM_BIN}: ${VIM_PATH}/.git ${PREFIX} ${LUA_BIN} ${LUAJIT_BIN}
	cd ${VIM_PATH}/src && ./configure ${VIM_FLAGS} && make -j2 && make install

${VIM_PATH}/.git: ${OPENSRC_PATH}
	mkdir -p ${VIM_PATH}
	cd ${VIM_PATH}; \
		if [ ! -e .git ]; then \
			git clone ${VIM_REPO} .; \
		fi; \
		git checkout ${VIM_TAG};


