
syntax enable

set encoding=utf8
set background=dark
set number
set backspace=indent,eol,start
set shiftwidth=2
set tabstop=2
set smarttab
set expandtab
set colorcolumn=81,121
set incsearch
set cursorline
set ignorecase
set smartcase
set autoindent
set smartindent
set list
set listchars=eol:\ ,tab:¬¬,trail:⚠
set linebreak
set mouse=a
set laststatus=2
set showcmd
set showmatch
set termguicolors
set so=3


call plug#begin('~/.vim/plugged')

" color scheme
Plug 'flazz/vim-colorschemes'

" bad and good wolf
Plug 'sjl/badwolf'

" file tree
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" word highlight
Plug 'osyo-manga/vim-brightest'

" asynchronous lint
Plug 'w0rp/ale'

" lightline
Plug 'itchyny/lightline.vim'

" gdscript
Plug 'orenjiakira/vim-gdscript'

" lua syntax highlighter
Plug 'tbastos/vim-lua'

" toml
Plug 'cespare/vim-toml'

" neo complete
Plug 'Shougo/neocomplete.vim'

" glsl
Plug 'tikhomirov/vim-glsl'

call plug#end()

" colorscheme
colorscheme badwolf

" key remap
nnoremap <C-S-RIGHT> gt
nnoremap <C-S-LEFT> gT
inoremap <C-S-RIGHT> <ESC>gt
inoremap <C-S-LEFT> <ESC>gT
nnoremap <UP> gk
nnoremap <DOWN> gj
if !has('gui_running')
  set ttimeoutlen=10
  augroup FastEscape
    autocmd!
    au InsertEnter * set timeoutlen=0
    au InsertLeave * set timeoutlen=1000
  augroup END
endif


" vim brightest config
let g:brightest#enable_insert_mode = 1
let g:brightest#pattern = '\w\+'

" neo complete
let g:neocomplete#enable_at_startup = 1

" nerdtree git plugin
noremap <F2> :NERDTreeToggle<CR>
inoremap <F2> <ESC>:NERDTreeToggle<CR>
vnoremap <F2> <ESC>:NERDTreeToggle<CR>
let g:NERDTreeGitIgnoreFilter = 1

